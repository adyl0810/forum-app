USE `forum`;
INSERT INTO `users` (email, password, username)
VALUES ('qqq@qqq.com', '$2a$12$k03rx0qYBVfpKhRqEIrX2.iOyxkn/3b02drKMRHmUmf02TORJEHnu', 'Guy'),
       ('www@www.com', '$2a$12$k03rx0qYBVfpKhRqEIrX2.iOyxkn/3b02drKMRHmUmf02TORJEHnu', 'Dude'),
       ('eee@eee.com', '$2a$12$k03rx0qYBVfpKhRqEIrX2.iOyxkn/3b02drKMRHmUmf02TORJEHnu', 'Bruh'),
       ('rrr@rrr.com', '$2a$12$k03rx0qYBVfpKhRqEIrX2.iOyxkn/3b02drKMRHmUmf02TORJEHnu', 'Boi');