USE `forum`;

CREATE TABLE `replies`
(
    `id`           INT AUTO_INCREMENT NOT NULL,
    `post_id`      INT                NOT NULL,
    `user_id`      INT                NOT NULL,
    `text`         VARCHAR(256)       NOT NULL,
    `date_created` DATETIME           NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_replies_chat`
        FOREIGN KEY (`post_id`)
            REFERENCES `posts` (`id`),
    CONSTRAINT `fk_replies_user`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
);