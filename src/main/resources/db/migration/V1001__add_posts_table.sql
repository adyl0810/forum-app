USE `forum`;

CREATE TABLE `posts`
(
    `id`           INT AUTO_INCREMENT NOT NULL,
    `name`         VARCHAR(128)       NOT NULL,
    `description`  VARCHAR(256)       NOT NULL,
    `date_created` DATETIME           NOT NULL,
    `user_id`      INT                NOT NULL,
    `replies_qty`  INT                NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_posts_user`
        FOREIGN KEY (user_id)
            REFERENCES `users` (`id`)
);