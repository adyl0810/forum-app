$(async () => {
    const repliesContainer = $('#replies-container')
    const userId = $('#userId').val()
    const postId = $('#postId').val()

    $('#add-reply-form').on('submit', async () => {
        await $.ajax({
            type : 'POST',
            url : 'http://localhost:8080/api/replies/',
            data : {
                postId : parseInt(postId),
                userId : parseInt(userId),
                text : $('#reply-text').val()
            }
        })
    })
})