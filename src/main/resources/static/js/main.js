

$(document).ready(function($) {
    $(function () {
        const token = $("meta[name='_csrf']").attr("content");
        const header = $("meta[name='_csrf_header']").attr("content");
        $(document).ajaxSend(function(e, xhr, options) {
            xhr.setRequestHeader(header, token);
        });
    });

    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    $('#loadNext').on('click', function () { //При клике по элементу с id=loadNext выполнять...
        $.ajax({
            url: "http://localhost:8080/api/posts", //Путь к файлу, который нужно подгрузить
            type: "GET",
            success: function (response) {
                $('#products-container').append(response); //Подгрузка внутрь блока с id=products-container
            }
        });
    });
});

