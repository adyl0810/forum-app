package kz.attractor.forum.domain.frontend;

import kz.attractor.forum.domain.post.PostDTO;
import kz.attractor.forum.domain.post.PostService;
import kz.attractor.forum.domain.reply.ReplyDTO;
import kz.attractor.forum.domain.reply.ReplyService;
import kz.attractor.forum.domain.user.UserDTO;
import kz.attractor.forum.domain.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@AllArgsConstructor
@RequestMapping
public class FrontendController {

    private final PostService postService;
    private final ReplyService replyService;
    private final PropertiesService propertiesService;
    private final UserService userService;


    @GetMapping
    public String index(Model model,
                        HttpServletRequest uriBuilder,
                        Principal principal,
                        Pageable pageable) {
        var posts = postService.getPosts(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(posts, propertiesService.getDefaultPageSize(), model, uri);
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "index";
    }

    @GetMapping("/{id}")
    public String getPost(@PathVariable Integer id,
                          Model model,
                          Principal principal,
                          Pageable pageable,
                          HttpServletRequest uriBuilder) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        var replies = replyService.getRepliesByPostId(pageable, id);
        var uri = uriBuilder.getRequestURI();
        constructPageable(replies, propertiesService.getDefaultPageSize(), model, uri);
        PostDTO post = postService.getPost(id);
        model.addAttribute("post", post);
        return "post";
    }

    @GetMapping("/add-post")
    public String addPostForm(Model model,
                          Principal principal) {
        if (principal != null) {
            UserDTO user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
        }
        return "add-post";
    }

    @PostMapping("/add-post")
    public String addPost(@RequestParam String name,
                          @RequestParam String description,
                          Principal principal) {
        UserDTO user = userService.getByEmail(principal.getName());
        postService.addPost(name, description, user.getId());
        return "redirect:/";
    }

    @PostMapping("/delete-post")
    public String deletePost(@RequestParam Integer id) {
        postService.deletePost(id);
        return "redirect:/";
    }


    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
}
