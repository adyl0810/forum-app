package kz.attractor.forum.domain.user;

import kz.attractor.forum.domain.exceptions.UserAlreadyExistsException;
import kz.attractor.forum.domain.exceptions.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder _encoder;

    public List<UserDTO> getUsers() {
        List<User> users = userRepository.findAll();
        return fromListToDtoList(users);
    }

    public UserDTO getUser(Integer id) {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return UserDTO.from(user);
    }

    public UserDTO register(UserRegisterForm form) {
        if (userRepository.existsByEmail(form.getEmail()))
            throw new UserAlreadyExistsException();
        var user = User.builder()
                .email(form.getEmail())
                .username(form.getName())
                .password(_encoder.encode(form.getPassword()))
                .build();
        userRepository.save(user);
        return UserDTO.from(user);
    }

    public UserDTO getByEmail(String email) {
        var user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        return UserDTO.from(user);
    }

    public boolean isUserAuthenticated(String email) {
        return userRepository.existsByEmail(email);
    }

    public List<UserDTO> fromListToDtoList(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(UserDTO.from(user));
        }
        return userDTOs;
    }
}
