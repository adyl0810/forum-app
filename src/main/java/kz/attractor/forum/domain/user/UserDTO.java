package kz.attractor.forum.domain.user;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class UserDTO {

    @NotNull
    private int id;

    @NotBlank
    @Size(min = 2, max = 128)
    private String username;

    @Email
    @NotBlank
    @Size(min = 5, max = 128)
    private String email;

    @NotNull
    private boolean active;

    @NotBlank
    @Size(min = 2, max = 128)
    private String role;


    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .active(user.isActive())
                .role(user.getRole())
                .build();
    }
}
