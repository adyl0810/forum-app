package kz.attractor.forum.domain.post;

import kz.attractor.forum.domain.user.User;
import kz.attractor.forum.domain.user.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public Page<PostDTO> getPosts(Pageable pageable) {
        return postRepository.findAllByOrderByDateCreatedAsc(pageable)
                .map(PostDTO::from);
    }

    public PostDTO getPost(int id) {
        Post post = postRepository.getById(id);
        return PostDTO.from(post);
    }

    public void addPost(String name, String description, Integer userId) {
        LocalDateTime time = LocalDateTime.now();
        User user = userRepository.getById(userId);
        Post post = new Post(name, description, time, user, 0);
        postRepository.save(post);
    }

    public void deletePost(Integer id) {
        postRepository.deleteById(id);
    }

    public void editPost(Integer id, String name, String description) {
        Post post = postRepository.getById(id);
        if (name != null)
            post.setName(name);
        if (description != null)
            post.setDescription(description);
        postRepository.save(post);
    }

}
