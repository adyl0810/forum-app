package kz.attractor.forum.domain.post;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/api/posts")
public class PostRestController {

    private final PostService postService;

    @GetMapping
    public List<PostDTO> getPosts(Pageable pageable) {
        return postService.getPosts(pageable).getContent();
    }
}
