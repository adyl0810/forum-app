package kz.attractor.forum.domain.post;

import kz.attractor.forum.domain.user.User;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@Table(name = "posts")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Post {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 256)
    @Column(length = 256)
    private String description;

    @NotNull
    @Column
    private LocalDateTime dateCreated;

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @PositiveOrZero
    @Column
    private int repliesQty;

    public Post(String name, String description, LocalDateTime dateCreated, User user, int repliesQty) {
        this.name = name;
        this.description = description;
        this.dateCreated = dateCreated;
        this.user = user;
        this.repliesQty = repliesQty;
    }


    public static Post from(PostDTO post) {
        return builder()
                .id(post.getId())
                .name(post.getName())
                .description(post.getDescription())
                .dateCreated(post.getDateCreated())
                .user(User.from(post.getUser()))
                .repliesQty(post.getRepliesQty())
                .build();
    }
}
