package kz.attractor.forum.domain.post;

import kz.attractor.forum.domain.user.UserDTO;
import lombok.*;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class PostDTO {

    @NotNull
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 256)
    private String description;

    @NotNull
    private LocalDateTime dateCreated;

    @NotNull
    private UserDTO user;

    @NotNull
    @PositiveOrZero
    private int repliesQty;

    public static PostDTO from(Post post) {
        return builder()
                .id(post.getId())
                .name(post.getName())
                .description(post.getDescription())
                .dateCreated(post.getDateCreated())
                .user(UserDTO.from(post.getUser()))
                .repliesQty(post.getRepliesQty())
                .build();
    }
}
