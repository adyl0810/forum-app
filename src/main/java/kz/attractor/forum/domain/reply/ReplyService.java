package kz.attractor.forum.domain.reply;

import kz.attractor.forum.domain.post.Post;
import kz.attractor.forum.domain.post.PostDTO;
import kz.attractor.forum.domain.post.PostRepository;
import kz.attractor.forum.domain.post.PostService;
import kz.attractor.forum.domain.user.User;
import kz.attractor.forum.domain.user.UserDTO;
import kz.attractor.forum.domain.user.UserService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReplyService {

    private final ReplyRepository replyRepository;
    private final PostRepository postRepository;
    private final UserService userService;
    private final PostService postService;

    public Page<ReplyDTO> getRepliesByPostId(Pageable pageable, Integer id) {
        return replyRepository.findAllByPostIdOrderByDateCreatedAsc(id, pageable)
                .map(ReplyDTO::from);
    }

    public List<Reply> getAllRepliesByPostId(Integer id) {
        return replyRepository.findAllByPostId(id);
    }

    public void addReply(Integer postId, Integer userId, String text) {
        PostDTO post = postService.getPost(postId);
        UserDTO user = userService.getUser(userId);
        LocalDateTime time = LocalDateTime.now();
        Reply reply = new Reply(Post.from(post), User.from(user), text, time);
        post.setRepliesQty(post.getRepliesQty()+1);
        postRepository.save(Post.from(post));
        replyRepository.save(reply);
    }
}
