package kz.attractor.forum.domain.reply;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReplyRepository extends JpaRepository<Reply, Integer> {
    Page<Reply> findAllByPostIdOrderByDateCreatedAsc(Integer id, Pageable pageable);
    List<Reply> findAllByPostId(Integer id);
}
