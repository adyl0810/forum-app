package kz.attractor.forum.domain.reply;

import kz.attractor.forum.domain.post.PostDTO;
import kz.attractor.forum.domain.user.UserDTO;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PACKAGE)
public class ReplyDTO {

    @NotNull
    private Integer id;

    @NotNull
    private PostDTO post;

    @NotNull
    private UserDTO user;

    @NotNull
    @Max(256)
    private String text;

    @NotNull
    private LocalDateTime dateCreated;

    public static ReplyDTO from(Reply reply) {
        return builder()
                .id(reply.getId())
                .post(PostDTO.from(reply.getPost()))
                .user(UserDTO.from(reply.getUser()))
                .text(reply.getText())
                .dateCreated(reply.getDateCreated())
                .build();
    }
}
