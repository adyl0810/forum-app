package kz.attractor.forum.domain.reply;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/api/replies")
public class ReplyRestController {

    private final ReplyService replyService;

    @GetMapping
    public Page<ReplyDTO> getRepliesForPost(@RequestParam Integer id,
                                            Pageable pageable) {
        return replyService.getRepliesByPostId(pageable, id);
    }

    @PostMapping
    public void addReplyToPost(@RequestParam Integer postId,
                               @RequestParam Integer userId,
                               @RequestParam String text) {
        replyService.addReply(postId, userId, text);
    }
}
