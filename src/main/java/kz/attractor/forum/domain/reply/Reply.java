package kz.attractor.forum.domain.reply;

import kz.attractor.forum.domain.post.Post;
import kz.attractor.forum.domain.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Table(name = "replies")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Reply {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Post post;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @NotNull
    @Max(256)
    @Column(length = 256)
    private String text;

    @NotNull
    @Column
    private LocalDateTime dateCreated;

    public Reply(Post post, User user, String text, LocalDateTime dateCreated) {
        this.post = post;
        this.user = user;
        this.text = text;
        this.dateCreated = dateCreated;
    }

    public static Reply from(ReplyDTO reply) {
        return builder()
                .id(reply.getId())
                .post(Post.from(reply.getPost()))
                .user(User.from(reply.getUser()))
                .text(reply.getText())
                .dateCreated(reply.getDateCreated())
                .build();
    }

}
